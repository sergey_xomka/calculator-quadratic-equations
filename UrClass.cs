﻿using System;

namespace nsConsoleApp02_MathUr
{
    class UrClass
    {
        Double a, b, c;

        public Double A
            {
            get {
                return a;
            }
            set {
                a = value;
            }
        }
        public Double B
        {
            get
            {
                return b;
            }
            set
            {
                b = value;
            }
        }
        public Double C
        {
            get
            {
                return c;
            }
            set
            {
                c = value;
            }
        }


        public UrClass()
        {
            this.a = Double.NaN;
            this.b = Double.NaN;
            this.c = Double.NaN;
        }

        public double Des() {
            double d;
            d = System.Math.Pow(b, 2) - 4 * a * c;
            return d;
        }

        public double[] Kor()
        {
            double des = Des();
            if (des < 0)
            {
                Console.WriteLine("Корней нет");
                return null;
            }
            else
            {
                double[] kor = new double[2];
                kor[0] = (-b - System.Math.Sqrt(des)) / (2 * a);
                kor[1] = (-b + System.Math.Sqrt(des)) / (2 * a);
                return kor;
            }
            
        }

        public Boolean checkLin()
        {
            return (a == 0) ?  true : false;
        }

        public Boolean checkKv()
        {
            return (b == 0) ? true : false;
        }

        public String LinAnswer()
        {
            double x = (-c) / b;
            String answer = "Коэффициент a равен нулю. \n" +
                "Решение линейного уравнения "+b+"x+"+c+"=0 :\n"+
                "x = "+ x;
            return answer;
        }

        public String KvAnswer()
        {
            double pre_x = (-c) / a;
            String answer = "Коэффициент b равен нулю. \n";
            if (pre_x < 0)
            {
                answer += "Решений уравнения " + a+ "x^2+"+c+"=0 в действительных числах нет";
            }
            else if (pre_x==0)
            {
                answer += "Решение уравнения " + a + "x^2+" + c + "=0 :";
                answer += "\n x = 0";
            }
            else
            {
                answer += "Решение уравнения " + a + "x^2+" + c + "=0 :";
                answer += "\n x = ±" + System.Math.Sqrt(pre_x);
            }
            return answer;
        }
    }
}
