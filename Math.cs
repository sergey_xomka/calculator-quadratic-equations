﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nsConsoleApp02_MathUr
{
    class Math
    {
        UrClass c;
        public void Start()
        {
            c = new UrClass();
            Process();
        }

        private void Process() {
            Console.WriteLine("Решение квадратных уравнений.\nВведите коэффициенты a, b, c " +
                "квадратного уравнения через пробел либо каждый с новой строки");
            ReadK();
        }

        private void ReadK()
        {
            String line_one = Console.ReadLine();
            String[] k;
            k = line_one.Split(Char.Parse(" "));

            Double[] abc;
            if (CheckValid(k, out abc)) // Если не валид, то ошибка и повторный ввод
            {
                if (abc.Length == 3)
                {
                    c.A = abc[0];
                    c.B = abc[1];
                    c.C = abc[2];
                    Result();
                }
                else
                {
                    if (c.A.Equals(Double.NaN))
                    {
                        c.A = abc[0];
                        ReadK();
                    }else if (c.B.Equals(Double.NaN))
                    {
                        c.B = abc[0];
                        ReadK();
                    }
                    else if (c.C.Equals(Double.NaN))
                    {
                        c.C = abc[0];
                        Result();
                    }
                }
            }
            else {
                Console.WriteLine("Ошибка, введите заново");
                ReadK();
            }

        }

        private Boolean CheckValid(String[] k, out Double[] abc)
        {
            abc = new Double[k.Length];
            if (k.Length > 0)
            {
                for(int i = 0; i < k.Length; i++)
                {

                    if (!Double.TryParse(k[i], out abc[i]))
                    {
                        return false;
                    };
                    //if (abc[i] == 0) {
                    //    return false;
                    //}
                }
            }
            return true;
        }
        private void Result()
        {
            if (c.checkLin() && c.checkKv())
            {
                Console.WriteLine("Коэффициенты a и b равны нулю, уравнение не верное.");
                Console.WriteLine("Ввести заново? (y/n)");
                if (Console.ReadLine() == "y")
                {
                    Start();
                    return;
                }
                else
                {
                    return;
                }
            }
            else if(c.checkLin() )
            {
                Console.WriteLine(c.LinAnswer());
                Console.WriteLine("Ввести заново? (y/n)");
                if (Console.ReadLine() == "y")
                {
                    Start();
                    return;
                }
                else
                {
                    return;
                }
            }
            else if (c.checkKv())
            {
                Console.WriteLine(c.KvAnswer());
                Console.WriteLine("Ввести заново? (y/n)");
                if (Console.ReadLine() == "y")
                {
                    Start();
                    return;
                }
                else
                {
                    return;
                }
            }
            else if (c.Des() < 0)
            {
                Console.WriteLine("Дискриминант отрицательный. Действительных корней нет");
                Console.WriteLine("Ввести заново? (y/n)");
                if (Console.ReadLine() == "y")
                {
                    Start();
                    return;
                }
                else
                {
                    return;
                }
            }
            else if (c.Des() > 0) {
                Console.WriteLine("Дискриминант положительный. Корни квадратного уравнения:");
                Console.WriteLine("x1 =" + System.Math.Round(c.Kor()[0], 2));
                Console.WriteLine("x2 =" + System.Math.Round(c.Kor()[1], 2));
                Console.WriteLine("Ввести заново? (y/n)");
                if (Console.ReadLine() == "y")
                {
                    Start();
                    return;
                }
                else
                {
                    return;
                }
            }
            else if (c.Des() == 0)
            {
                Console.WriteLine("Дискриминант равен нулю. Корни квадратного уравнения:");
                Console.WriteLine("x1, x2=" + System.Math.Round(c.Kor()[0], 2));
                Console.WriteLine("Ввести заново? (y/n)");
                if (Console.ReadLine() == "y")
                {
                    Start();
                    return;
                }
                else
                {
                    return;
                }
            }
        }
    }
}
